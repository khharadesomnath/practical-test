import React, { Component, Fragment } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import landingRoutes from "../../routes/App.jsx";
import { Link } from 'react-router-dom'
import ErrorBoundaryComponent from "../../modules/error-boundary/ErrorBoundaryComponent.jsx"

class AppPage extends Component {
  render() {
    return (
      <Fragment>
        <div className="content">
          <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="container">
              <Link to={'#'} role="button" onClick={() => {
                // localStorage.clear();
                localStorage.removeItem("email");
                setTimeout(() => {
                  this.props.history.push('/');
                }, 1000)
              }} className="navbar-brand"  >ARTICLE</Link>
            </div>
          </nav>
          <Switch>
            {landingRoutes.map((prop, key) => {
              if (prop.redirect)
                return <Redirect from={prop.path} to={prop.pathTo} key={key} />;
              else
                return (
                  <Route
                    path={prop.path}
                    component={prop.component}
                    key={key}
                  >
                    <ErrorBoundaryComponent>
                      <prop.component
                        {...this.props}
                      />
                    </ErrorBoundaryComponent>

                  </Route>
                );
            })}
          </Switch>
        </div>
      </Fragment>
    );
  }
}

export default AppPage;
