import React, { Component, Fragment } from 'react';
import { Form } from 'react-bootstrap';
import '../../../assets/css/login.css';
import PasswordField from 'material-ui-password-field'
import { toast } from "react-toastify";
import loader from "../../../assets/images/loader.gif";
import SimpleReactValidator from 'simple-react-validator';
import { ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import Button from '@material-ui/core/Button';

export default class LoginPage extends Component {
    constructor(props) {
        super(props)
        this.validator = new SimpleReactValidator({
            messages: {
                email: "Invaild email address",
            }
        });
        this.state = {
            loading: false,
            user: {
                email: "",
                password: "",

            },
        }

    }
    componentDidMount() {
        window.scrollTo(0, 0)
    }
    onLogin = () => {
        let _this = this;
        let tempUser = JSON.parse(JSON.stringify(this.state.user));
        if (_this.validator.allValid()) {
            _this.setState({ loading: true })
            if (tempUser.email === 'test@yopmail.com' && tempUser.password === '123456') {
                _this.setState({ loading: false })
                localStorage.removeItem("email");
                localStorage.setItem('email', tempUser.email);
                toast.success('Logged in sucessfully!', { position: "top-right", autoClose: 2000 });
                setTimeout(() => {
                    _this.props.history.push('/articles');
                }, 2000)
            } else {
                toast.error('Invalid email or password!', { position: "top-right", autoClose: 5000 });
                _this.setState({ loading: false })
            }

        } else {
            _this.validator.showMessages();
            this.forceUpdate();
        }
    }
    handleInputChange = (e) => {
        var newObj = this.state.user;
        newObj[e.target.name] = e.target.value;
        this.setState({ user: newObj });
    }
    render() {
        let _this = this;
        let form = (
            <section className="login-section">
                <div className="form-block">
                    <h2>Login Page</h2>
                    <Form>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Email</Form.Label>
                            <input
                                type="email"
                                placeholder="Enter email"
                                name="email"
                                className="input form-control"
                                value={_this.state.user && _this.state.user.email}
                                onChange={this.handleInputChange}
                                onKeyPress={event => {
                                    if (event.key === "Enter") {
                                        _this.onLogin();
                                    }
                                }}
                            />
                            <div className="errorMsg">{_this.validator.message('Email', (_this.state.user && _this.state.user.email), 'required|email')}</div>
                        </Form.Group>
                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <PasswordField
                                classes={{
                                    root: "input form-control"
                                }}
                                placeholder="Enter password"
                                hintText="At least 6 characters"
                                errorText="Your password is too short"
                                id="password"
                                name="password"
                                value={_this.state.user && _this.state.user.password}
                                onChange={this.handleInputChange}
                                onKeyPress={event => {
                                    if (event.key === "Enter") {
                                        _this.onLogin();
                                    }
                                }}
                            />
                            <div className="errorMsg">{_this.validator.message('password', (_this.state.user && _this.state.user.password), 'required|min:6')}</div>
                        </Form.Group>
                        <Button variant="contained" color="primary" onClick={() => _this.onLogin()} className="btn-block btn" >Submit</Button>
                    </Form>
                </div>
            </section>
        )
        return (
            <Fragment>
                <ToastContainer />
                {this.state.loading
                    ? (
                        <div className="modal-backdrop1 in">
                            <img src={loader} alt="loader" className="preLoader" />
                            {form}
                        </div>
                    ) : (
                        form
                    )
                }
            </Fragment>
        )
    }
}
