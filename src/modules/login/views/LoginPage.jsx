import React, { Component } from "react";
import LoginPageComponent from "../components/LoginPageComponent.jsx";

class LoginPage extends Component {
    render() {
        return <LoginPageComponent {...this.props}></LoginPageComponent>;
    }
}

export default LoginPage;
