import React, { useState, useEffect, Fragment } from 'react'
import { Link } from 'react-router-dom'
import "../../../assets/css/articale.css"
import loader from "../../../assets/images/loader.gif";
import { ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import { fetchArticleList } from "../server/ArticleServer.js";
import { useHistory } from 'react-router-dom'
import Button from '@material-ui/core/Button';

export default function ArticleListComponent() {
    const [data, setData] = useState([])
    const [loading, setLoading] = useState(false);
    const history = useHistory();
    
    useEffect(() => {
        window.scrollTo(0, 0);
        if (localStorage.getItem("email")) {
            fetchDataFromServer();
        } else {
            history.push('/login');
        }
    }, [])
    const fetchDataFromServer = () => {
        setLoading(true)
        fetchArticleList("",
            (data) => {
                let newObj = data;
                const tempObj = JSON.parse(localStorage.getItem("article"));
                if (tempObj && tempObj.length > 0) {
                    newObj = [...newObj,...tempObj];
                    setData(newObj);
                } else {
                    setData(newObj);
                }
                setLoading(false)
            },
            () => {
                setLoading(false)
            }
        );
    }
    const form = (
        <Fragment>
            <section className="artical-list-section">
                <h2 className="title">Article List</h2>
                <Button variant="contained" color="primary" onClick={() =>history.push('/create-article')} className="btn-block">Add Article</Button>
                <div className="block">
                    {
                        data && data.length > 0 && data.map((prop, index) => {
                            return (
                                <div className="artical-block">
                                    <Link role="button" to={`/article-details/${index + 1}`}>
                                        {
                                            (prop.title && prop.title.length > 200)
                                                ? (
                                                    <p>{`${prop.title && prop.title.substring(0, 200)} ...`}</p>
                                                ) :
                                                (
                                                    <p>{prop.title}</p>
                                                )
                                        }

                                    </Link>
                                </div>
                            )
                        })
                    }
                </div>
            </section>
        </Fragment>
    )
    return (
        <Fragment>
            <ToastContainer />
            {loading
                ? (
                    <div className="modal-backdrop1 in">
                        <img src={loader} alt="loader" className="preLoader" />
                        {form}
                    </div>
                ) : (
                    form
                )
            }
        </Fragment>
    )

}
