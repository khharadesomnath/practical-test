import React, { Component, Fragment } from 'react';
import { Form } from 'react-bootstrap';
import '../../../assets/css/login.css';
import { toast } from "react-toastify";
import loader from "../../../assets/images/loader.gif";
import SimpleReactValidator from 'simple-react-validator';
import { ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import { createArticle } from "../server/ArticleServer.js";
import { Box } from "@material-ui/core";
import Button from '@material-ui/core/Button';

export default class AddEditArticleComponent extends Component {
    constructor(props) {
        super(props)
        this.validator = new SimpleReactValidator({
            messages: {}
        });
        this.state = {
            loading: false,
            data: [],
            article: {
                title: "",
                body: "",
            },
        }

    }
    componentDidMount() {
        window.scrollTo(0, 0)
        const articleData = JSON.parse(localStorage.getItem("article"));
        if (articleData !== null) {
            this.setState({ data: articleData
})
        }
    }
    onSubmit = () => {
        let _this = this;
        let tempObj = JSON.parse(JSON.stringify(this.state.article));
        tempObj.userId = 1;
        if (_this.validator.allValid()) {
            _this.setState({ loading: true });
                createArticle(
                    tempObj,
                    (res) => {
                        const Obj = {
                            title: tempObj.title,
                            body: tempObj.body,
                            userId:1 
                        };
                        const newObj = [..._this.state.data, Obj]
                        localStorage.setItem("article", JSON.stringify(newObj));
                        _this.setState({
                            loading: false,
                            data: newObj,
                        });
                        toast.success('Article added successfully!', { position: "top-right", autoClose: 2000 });

                        setTimeout(() => _this.props.history.push("/articles"), 3000);
                    },
                    () => {
                        _this.setState({ loading: false, showAlert: false });
                    }
                );
        } else {
            _this.validator.showMessages();
            this.forceUpdate();
        }
    }
    handleInputChange = (e) => {
        var newObj = this.state.article;
        newObj[e.target.name] = e.target.value;
        this.setState({ article: newObj });
    }
    render() {
        let _this = this;
        let form = (
            <section className="login-section">
                <div className="form-block">
                    <h2>Add Article</h2>
                    <Form>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Group controlId="exampleForm.ControlInput1">
                                <Form.Label>Title<span className="star">*</span></Form.Label>
                                <Form.Control type="text"
                                    placeholder="Enter title"
                                    name="title"
                                    value={_this.state.article.title}
                                    onChange={this.handleInputChange}
                                />
                            </Form.Group>
                            <div className="errorMsg">{_this.validator.message('title', (_this.state.article.title), 'required')}</div>
                        </Form.Group>    
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Body<span className="star">*</span></Form.Label>
                           
                            <Form.Group controlId="exampleForm.ControlTextarea1">
                                <Form.Control
                                    as="textarea"
                                    rows={5} 
                                    placeholder="Enter body"
                                 value={_this.state.article.body}
                                 onChange={this.handleInputChange}
                                 name="body"
                                />
                            </Form.Group>
                            <div className="errorMsg">{_this.validator.message('body', (_this.state.article.body), 'required')}</div>
                        </Form.Group>    
                        <Box className="form_btn_box">
                            <Button type="button" onClick={() => _this.props.history.goBack()} variant="contained" color="default" className="btn-block " >Back</Button>
                            <Button variant="contained" color="primary" onClick={() => _this.onSubmit()} className="btn-block btn" >Submit</Button>
                            </Box>
                       
                    </Form>
                </div>
            </section>
        )
        return (
            <Fragment>
                <ToastContainer />
                {this.state.loading
                    ? (
                        <div className="modal-backdrop1 in">
                            <img src={loader} alt="loader" className="preLoader" />
                            {form}
                        </div>
                    ) : (
                        form
                    )
                }
            </Fragment>
        )
    }
}
