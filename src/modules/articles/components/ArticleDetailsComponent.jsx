import React, { useState, Fragment, useEffect } from 'react'
import { Card } from 'react-bootstrap'
import Button from '@material-ui/core/Button';
import { useHistory } from 'react-router-dom'
import { fetchSingleArticle } from "../server/ArticleServer.js";
import loader from "../../../assets/images/loader.gif";

export default function ArticleDetailsComponent(props) {
    const [articleDetail, setArticleDetail] = useState({})
    const [loading, setLoading] = useState(false);
    const history = useHistory();

    useEffect(() => {
        window.scrollTo(0, 0);
        if (props.location.pathname.includes("/article-details/")) {
            let url = props.location.pathname.split("/article-details/");
            let articleId = (url && url[url.length - 1])
            if (articleId <= 100) {
                fetchDataFromServer(articleId);
            }
            else {
                getLocalData(articleId);
            }
        } else {
            history.push('/articles');
        }

    }, [])

    const fetchDataFromServer = (id) => {
        setLoading(true)
        fetchSingleArticle(id,
            (res) => {
                setArticleDetail(res)
                setLoading(false)
            },
            () => {
                setLoading(false)
            }
        );
    }
    const getLocalData = (id) => {
        setLoading(true)
        const tempObj = JSON.parse(localStorage.getItem("article"));
        const newData = tempObj.filter((prop, key) => {return (key === (id - 100) - 1)})
        setLoading(false)
        setArticleDetail(newData[0]);
    }
    const form = (
        <Fragment>
            <section className="artical-detail-section">
                <h2 className="title">Article Details Page</h2>
                <div className="block">
                    <Card>
                        <Card.Body>
                            <Card.Title>{articleDetail && articleDetail.title}</Card.Title>
                            <Card.Text>
                                {articleDetail && articleDetail.body}
                            </Card.Text>
                            <Button type="button" onClick={() => props.history.goBack()} variant="contained" color="default" className="btn-block" >Back</Button>
                        </Card.Body>
                    </Card>
                </div>
            </section>
        </Fragment>
    )
    return (
        <Fragment>
            {
                loading
                    ? (
                        <div className="modal-backdrop1 in">
                            <img src={loader} alt="loader" className="preLoader" />
                            {form}
                        </div>
                    ) : (
                        form
                    )
            }
        </Fragment>
    )
}
