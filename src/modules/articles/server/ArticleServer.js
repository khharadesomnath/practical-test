import axios from "axios";
import { backendURL } from "../../../variables/appVariables.jsx";


export function fetchArticleList(params, success, error) {
    axios.get((`${backendURL}posts`),
        {
            headers: {
                'Content-Type': 'application/json',
                "Access-Control-Allow-Origin": "*",
            }
        })
        .then(res => {
            if (res.status === 200) {
                success(res.data);
            } else {
                error(res.data);
            }
        }).catch((error) => {
           
        })
}
export function fetchSingleArticle(id, success, error) {
    axios.get((`${backendURL}posts/${id}`), 
        {
            headers: {
                'Content-Type': 'application/json',
                "Access-Control-Allow-Origin": "*",
            }
        })
        .then(res => {
            if (res.status === 200) {
                success(res.data);
            } else {
                error(res.data);
            }
        }).catch((error) => {

        })
}
export function createArticle(data, success, error) {
    axios.post((`${backendURL}posts`), data,
        {
            headers: {
                'Content-Type': 'application/json',
                "Access-Control-Allow-Origin": "*",
            }
        })
        .then(res => {
            if (res.status === 201) {
                success(res.data);
            } else {
                error(res.data);
            }
        }).catch((error) => {

        })
}