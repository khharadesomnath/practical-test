import React, { Component } from "react";
import ArticleListComponent from "../components/ArticleListComponent.jsx";

class ArticleListPage extends Component {
    render() {
        return <ArticleListComponent {...this.props}></ArticleListComponent>;
    }
}

export default ArticleListPage;
