import React, { Component } from "react";
import ArticleDetailsComponent from "../components/ArticleDetailsComponent.jsx";

class ArticleDetailsPage extends Component {
    render() {
        return <ArticleDetailsComponent {...this.props}></ArticleDetailsComponent>;
    }
}

export default ArticleDetailsPage;
