import React, { Component } from "react";
import AddEditArticleComponent from "../components/AddEditArticleComponent.jsx";

class AddEditArticlePage extends Component {
    render() {
        return <AddEditArticleComponent {...this.props}></AddEditArticleComponent>;
    }
}

export default AddEditArticlePage;
