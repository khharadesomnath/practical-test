import LoginPage from "../modules/login/views/LoginPage.jsx";
import ArticleListPage from "../modules/articles/views/ArticleListPage.jsx";
import AddEditArticlePage from "../modules/articles/views/AddEditArticlePage.jsx";
import ArticleDetailsPage from "../modules/articles/views/ArticleDetailsPage.jsx";

var userRoutes = [
  { path: "/login", component: LoginPage },
  { path: "/ ", component: LoginPage },
  { path: "/articles", component: ArticleListPage },
  { path: "/create-article", component: AddEditArticlePage },
  { path: "/article-details/:id", component: ArticleDetailsPage },
  { redirect: true, path: "/", pathTo: "/ " },
];

export default userRoutes;
